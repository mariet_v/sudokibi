##
## Makefile for sudoku in /home/mariet_v//rush/rush3/sudokibi
## 
## Made by valentin mariette
## Login   <mariet_v@epitech.net>
## 
## Started on  Sun Apr 21 15:27:28 2013 valentin mariette
## Last update Sun Apr 21 15:43:12 2013 valentin mariette
##

all:
	@(cd etape1 && $(MAKE))
	@(cd etape2 && $(MAKE))

re:	
	make re -C etape1
	make re -C etape2

clean:
	make clean -C etape1
	make clean -C etape2

fclean:
	make fclean -C etape1
	make fclean -C etape2