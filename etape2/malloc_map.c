/*
** malloc_map.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Apr 19 22:04:39 2013 valentin mariette
** Last update Sun Apr 21 13:03:31 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

char	***malloc_map(void)
{
  char	***map;
  int	i;
  int	j;

  i = 0;
  if (!(map = malloc(sizeof(char **) * 9)))
    error(1);
  while (i < 9)
    {
      if (!(map[i] = malloc(sizeof(char *) * 9)))
	error(1);
      j = 0;
      while (j < 9)
	{
	  if (!(map[i][j] = malloc(sizeof(char) * 10)))
	    error(1);
	  j++;
	}
      i++;
    }
  return (map);
}

void	free_map(t_sudoku *sudoku)
{
  int	i;
  int	j;

  i = 0;
  while (i < 9)
    {
      j = 0;
      while (j < 9)
	{
	  free(sudoku->workmap[i][j]);
	  j++;
	}
      free(sudoku->workmap[i]);
      i++;
    }
  free(sudoku->workmap);
  free(sudoku);
}
