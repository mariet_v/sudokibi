/*
** my_strdup.c for printf in /home/mariet_v//celem/printf
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Nov 17 14:16:05 2012 valentin mariette
** Last update Tue Feb 26 14:19:28 2013 valentin mariette
*/

#include <stdlib.h>

char    *my_strdup(char *src)
{
  char  *str;
  int   i;

  i = 0;
  str = malloc(my_strlen(src) + 1);
  while (src[i] != '\0')
    {
      str[i] = src[i];
      i++;
    }
  str[i] = '\0';
  return (str);
}
