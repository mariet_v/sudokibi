/*
** exo1.c for exo1 in /home/mariet_v//day3
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Wed Oct  3 11:08:28 2012 valentin mariette
** Last update Tue Feb 26 14:18:30 2013 valentin mariette
*/

#include <stdlib.h>
#include <stdio.h>

int     puissance(int a, int b)
{
  int   c;

  c = a;
  if (b == 0)
    c = 1;
  while (b > 1)
    {
      c = c * a;
      b--;
    }
  return (c);
}

int	get_nbr_char(int nbr)
{
  int	i;

  i = 0;
  while (nbr > 9)
    {
      nbr /= 10;
      i++;
    }
  return (i + 1);
}

int	my_put_nbr(int nbr)
{
  int	i;
  int	nbr_char;
  int	cur_char;
  int	retenue;
  int	a;

  retenue = 0;
  i = 0;
  a = 0;
  if (nbr < 0)
    {
      a = 1;
      my_putchar('-');
      nbr = -nbr;
    }
  nbr_char = get_nbr_char(nbr);
  while (i < nbr_char)
    {
      cur_char = nbr / (puissance(10, nbr_char - i - 1)) - (retenue * 10);
      my_putchar(cur_char + 48);
      retenue = retenue * 10 + cur_char;
      i++;
    }
  return (nbr_char + a);
}
