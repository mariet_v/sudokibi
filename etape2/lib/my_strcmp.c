/*
** exo1.c for my_strcpy in /home/mariet_v//day6
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Oct  8 09:46:21 2012 valentin mariette
** Last update Mon Oct  8 17:15:31 2012 valentin mariette
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] == s2[i] && s1[i] != '\0')
    {
      i++;
    }
  return s1[i] - s2[i];
}
