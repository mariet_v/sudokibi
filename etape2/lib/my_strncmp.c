/*
** exo1.c for my_strcpy in /home/mariet_v//day6
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Oct  8 09:46:21 2012 valentin mariette
** Last update Tue Feb 26 14:20:09 2013 valentin mariette
*/

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (s1[i] == s2[i] && s1[i] != '\0' && i < n - 1)
    {
      i++;
    }
  return (s1[i] - s2[i]);
}
