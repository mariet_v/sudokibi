/*
** check_case.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 00:36:45 2013 valentin mariette
** Last update Sun Apr 21 13:11:01 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	check_case(char ***map, int i, int j)
{
  if (map[i][j][0] > 0)
    set_to_zero(map[i][j]);
  check_line(map, i, j);
  check_columns(map, i, j);
  check_region(map, i, j);
}

void	check_line(char ***map, int i, int j)
{
  int	x;

  x = 0;
  while (x < 9)
    {
      if (map[i][x][0] > 0)
	map[i][j][map[i][x][0]] = 0;
      x++;
    }
  return;
}

void	check_columns(char ***map, int i, int j)
{
  int	y;

  y = 0;
  while (y < 9)
    {
      if (map[y][j][0] > 0)
        map[i][j][map[y][j][0]] = 0;
      y++;
    }
  return;
}

void	check_region(char ***map, int i, int j)
{
  int	topleftx;
  int	toplefty;
  int	x;
  int	y;

  y = 0;
  get_topleft_region(i, j, &topleftx, &toplefty);
  while (y < 3)
    {
      x = 0;
      while (x < 3)
	{
	  if (map[toplefty + y][topleftx + x][0] > 0)
	    map[j][i][map[toplefty + y][topleftx + x][0]] = 0;
	  x++;
	}
      y++;
    }
  return;
}
