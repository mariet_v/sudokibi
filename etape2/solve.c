/*
** solve.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 09:57:59 2013 valentin mariette
** Last update Sun Apr 21 12:59:58 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	solve(t_sudoku *sudoku)
{
  if (simple(sudoku))
    aff_sudoku(sudoku->workmap);
  else if (diabolico(sudoku))
    aff_sudoku(sudoku->workmap);
  else
    error(2);
}

int	simple(t_sudoku *sudoku)
{
  int	i;
  int	j;
  int	carry;

  carry = 1;
  while (carry)
    {
      carry = 0;
      i = 0;
      j = 0;
      while (i < 9)
	{
	  j = 0;
	  while (j < 9)
	    {
	      one_soluce(sudoku->workmap[i][j], &carry);
	      find_possibilities(sudoku);
	      j++;
	    }
	  i++;
	}
    }
  return (is_good(sudoku->workmap));
}

int	diabolico(t_sudoku *sudoku)
{
  recurs(sudoku->workmap, 0, 0);
  simple(sudoku);
  if (sudoku->workmap[8][8][0] == 0)
    sudoku->workmap[8][8][0] = 2;
  return (is_good(sudoku->workmap));
}
