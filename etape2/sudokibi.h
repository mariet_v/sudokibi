/*
** sudokibi.h for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Apr 19 20:41:42 2013 valentin mariette
** Last update Sun Apr 21 17:26:42 2013 valentin mariette
*/

#ifndef SUDOKIBI_H_
# define SUDOKIBI_H_

# include "get_next_line.h"

typedef struct	s_sudoku
{
  char		***workmap;
  char		is_ok;
}		t_sudoku;

t_sudoku	*get_map(void);
char		***malloc_map(void);
void		pars_line(char *line, char **map);
void		set_to_zero(char *foo);
void		set_to_one(char *foo);
void		check_case(char ***map, int i, int j);
void		find_possibilities(t_sudoku *sudoku);
void		check_line(char ***map, int i, int j);
void		check_columns(char ***map, int i, int j);
void		check_region(char ***map, int i, int j);
void		get_topright_region(int i, int j, int *toprightx, int *topright);
int		simple(t_sudoku *sudoku);
void		solve(t_sudoku *sudoku);
void		one_soluce(char *box, int *carry);
void		aff_sudoku(char ***sudoku);
int		is_good(char ***map);
int		diabolico(t_sudoku *sudoku);
int		nexti(int i, int j);
int		nextj(int i, int j);
int		no_region(char ***map, int i, int j, int k);
int		no_columns(char ***map, int i, int j, int k);
int		no_line(char ***map, int i, int j, int k);
int		recurs(char ***map, int i, int j);
void		top_left(int    i, int j, int *topleftx, int *toplefty);
void		free_map(t_sudoku *sudoku);
void		error(int flag);
void		line_check(char *line);
#endif
