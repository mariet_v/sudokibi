/*
** is_finish.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 10:52:05 2013 valentin mariette
** Last update Sat Apr 20 11:01:19 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

int	is_good(char ***map)
{
  int	i;
  int	j;

  i = 0;
  while (i < 9)
    {
      j = 0;
      while (j < 9)
	{
	  if (map[i][j][0] == 0)
	    return (0);
	  j++;
	}
      i++;
    }
  return (1);
}
