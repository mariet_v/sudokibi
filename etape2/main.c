/*
** main.c for sudoki-bi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Apr 19 20:29:17 2013 valentin mariette
** Last update Sun Apr 21 16:46:19 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

int		main(int argc, char **argv)
{
  t_sudoku	*sudoku;
  char		begin;

  begin = 0;
  while (1)
    {
      sudoku = get_map();
      if (begin == 0)
	begin = 1;
      else if (sudoku->is_ok == 1)
	my_putstr("####################\n");
      if (sudoku->is_ok == 0)
	return (1);
      find_possibilities(sudoku);
      solve(sudoku);
      free_map(sudoku);
    }
  return (1);
}
