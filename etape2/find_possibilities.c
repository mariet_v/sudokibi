/*
** find_possibilities.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 00:24:07 2013 valentin mariette
** Last update Sat Apr 20 00:46:26 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	find_possibilities(t_sudoku *sudoku)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (i < 9)
    {
      j = 0;
      while (j < 9)
	{
	  check_case(sudoku->workmap, i, j);
	  j++;
	}
      i++;
    }
}
