/*
** no_here.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 17:44:35 2013 valentin mariette
** Last update Sat Apr 20 19:50:34 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

int	no_line(char ***map, int i, int j, int k)
{
  j = 0;
  while (j < 9)
    {
      if (map[i][j][0] == k)
	return (0);
      j++;
    }
  return (1);
}

int	no_columns(char ***map, int i, int j, int k)
{
  i = 0;
  while (i < 9)
    {
      if (map[i][j][0] == k)
	return (0);
      i++;
    }
  return (1);
}

int	no_region(char ***map, int i, int j, int k)
{
  int	topleftx;
  int	toplefty;
  int	x;
  int	y;

  y = 0;
  top_left(i, j, &topleftx, &toplefty);
  while (y < 3)
    {
      x = 0;
      while (x < 3)
	{
	  if (map[toplefty + y][topleftx + x][0] == k)
	    return (0);
	  x++;
	}
      y++;
    }
  return (1);
}
