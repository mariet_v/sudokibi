/*
** my.h for mysh in /home/mariet_v//celem/mysh/lib
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Feb 26 14:16:02 2013 valentin mariette
** Last update Sat Mar  2 09:27:58 2013 valentin mariette
*/

#ifndef MY_H_
# define MY_H_

void	my_putchar(char c);
void	my_putstr(char *str);
int	my_strlen(char *str);
int     my_put_nbr(int nbr);
int     my_strcmp(char *s1, char *s2);
char    *my_strdup(char *src);
int     my_strncmp(char *s1, char *s2, int n);
char    *my_strnndup(char *src, int first, int last);
#endif
