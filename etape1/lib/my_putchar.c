/*
** my_putchar.c for my_putchar in /home/mariet_v//day7
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Oct  9 09:57:14 2012 valentin mariette
** Last update Tue Oct  9 09:57:50 2012 valentin mariette
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
