/*
** error.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Apr 19 21:07:10 2013 valentin mariette
** Last update Sun Apr 21 17:15:00 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	error(int flag)
{
  if (flag == 1)
    my_putstr("Error on Malloc\n");
  else if (flag == 2)
    my_putstr("Map error\n");
  exit(-1);
}
