/*
** get_map.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Apr 19 21:02:52 2013 valentin mariette
** Last update Sun Apr 21 13:12:10 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

t_sudoku	*get_map(void)
{
  t_sudoku	*sudoku;
  char		*line;
  int		i;

  i = 0;
  if (!(sudoku = malloc(sizeof(t_sudoku))))
    error(1);
  sudoku->workmap = malloc_map();
  while (line = get_next_line(0))
    {
      if (my_strlen(line) == 20 && line[1] != '-')
	pars_line(line, sudoku->workmap[i++]);
      free(line);
    }
  return (sudoku);
}

void	pars_line(char *line, char **map)
{
  int	i;
  int	j;

  i = 2;
  j = 0;
  while (j < 9)
    {
      if (line[i] == ' ')
	map[j][0] = 0;
      else
	map[j][0] = line[i] - 48;
      set_to_one(map[j]);
      j++;
      i += 2;
    }
  return;
}

void	set_to_zero(char *foo)
{
  int	i;

  i = 1;
  while (i < 10)
    foo[i++] = 0;
  return;
}

void	set_to_one(char *foo)
{
  int	i;

  i = 1;
  while (i < 10)
    foo[i++] = 1;
  return;
}
