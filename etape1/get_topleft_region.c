/*
** get_center_region.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 01:05:35 2013 valentin mariette
** Last update Sat Apr 20 19:48:51 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	get_topleft_region(int i, int j, int *topleftx, int *toplefty)
{
  if (i <= 2)
    *topleftx = 0;
  else if (i <= 5)
    *topleftx = 3;
  else
    *topleftx = 6;
  if (j <= 2)
    *toplefty = 0;
  else if (j <= 5)
    *toplefty = 3;
  else
    *toplefty = 6;
  return;
}

void	top_left(int	i, int j, int *topleftx, int *toplefty)
{
  if (i <= 2)
    *toplefty = 0;
  else if (i <= 5)
    *toplefty = 3;
  else
    *toplefty = 6;
  if (j <= 2)
    *topleftx = 0;
  else if (j <= 5)
    *topleftx = 3;
  else
    *topleftx = 6;
  return;

}
