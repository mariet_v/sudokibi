/*
** main.c for sudoki-bi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Apr 19 20:29:17 2013 valentin mariette
** Last update Sun Apr 21 13:13:01 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

int		main(int argc, char **argv)
{
  t_sudoku	*sudoku;

  sudoku = get_map();
  find_possibilities(sudoku);
  solve(sudoku);
  free_map(sudoku);
  return (1);
}
