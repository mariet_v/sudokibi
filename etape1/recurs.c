/*
** recurs.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 17:02:30 2013 valentin mariette
** Last update Sun Apr 21 13:07:14 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

int	recurs(char ***map, int i, int j)
{
  int	k;

  if (i == 8 && j == 8)
    return (1);
  k = 1;
  if (map[i][j][0] != 0)
    return (recurs(map, nexti(i, j), nextj(i, j)));
  while (k <= 9)
    {
      if (no_line(map, i, j, k) && no_columns(map, i, j, k) &&
	  no_region(map, i, j, k))
	{
	  map[i][j][0] = k;
	  if (recurs(map, nexti(i, j), nextj(i, j)))
	    return (1);
	}
      k++;
    }
  map[i][j][0] = 0;
  return (0);
}

int	nexti(int i, int j)
{
  j++;
  if (j == 9)
    i++;
  return (i);
}

int	nextj(int i, int j)
{
  j++;
  if (j == 9)
    j = 0;
  return (j);
}
