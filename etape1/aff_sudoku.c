/*
** aff_sudoku.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 10:37:51 2013 valentin mariette
** Last update Sat Apr 20 10:44:57 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	aff_sudoku(char ***sudoku)
{
  int	i;
  int	j;

  i = 0;
  my_putstr("|------------------|\n");
  while (i < 9)
    {
      j = 0;
      my_putchar('|');
      while (j < 9)
	{
	  my_putchar(' ');
	  my_put_nbr(sudoku[i][j][0]);
	  j++;
	}
      my_putchar('|');
      my_putchar('\n');
      i++;
    }
  my_putstr("|------------------|\n");
}
