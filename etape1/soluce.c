/*
** soluce.c for sudokibi in /home/mariet_v//rush/rush3/sudokibi/etape1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Apr 20 10:22:01 2013 valentin mariette
** Last update Sat Apr 20 10:36:11 2013 valentin mariette
*/

#include <stdlib.h>
#include "sudokibi.h"

void	one_soluce(char *box, int *carry)
{
  int	nbr;
  int	i;
  int	good;

  i = 1;
  nbr = 0;
  while (i < 10)
    {
      if (box[i] > 0)
	nbr++;
      i++;
    }
  if (nbr == 1)
    {
      i = 1;
      while (i < 10)
	{
	  if (box[i] > 0)
	    good = i;
	  i++;
	}
      box[0] = good;
      set_to_zero(box);
      *carry = 1;
    }
}
